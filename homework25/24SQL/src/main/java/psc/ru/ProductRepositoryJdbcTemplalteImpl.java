package psc.ru;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class ProductRepositoryJdbcTemplalteImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BU_PRICE = "select * from product where price >= ? order by id";

    private JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplalteImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String title = row.getString("title");
        double price = row.getDouble("price");
        int quantity = row.getInt("quantity");

        return new Product(id, title, price, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query (SQL_SELECT_ALL_BU_PRICE, productRowMapper, price);

    }


}
