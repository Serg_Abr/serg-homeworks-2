package psc.ru;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/psc2","postgres", "qwer01");

        ProductRepository productRepository = new ProductRepositoryJdbcTemplalteImpl(dataSource);
        System.out.println(productRepository.findAll());
        System.out.println(productRepository.findAllByPrice(45));
    }
}
